// Copyright (c) 2020, Dokos SAS and Contributors
// See license.txt

import "frappe/public/js/frappe/ui/web_calendar";

erpnext.itemBookingCalendar = class ItemBookingCalendar {
	constructor(opts) {
		Object.assign(this, opts);
		this.calendar = {};
		this.wrapper = document.getElementById(this.parentId);
		this.show()
	}

	show() {
		this.build_calendar()
	}

	build_calendar() {
		this.calendar = new ItemCalendar({
			wrapper: this.wrapper,
			parent: this,
		})
	}
}

class ItemCalendar extends frappe.ui.BaseWebCalendar {
	init(opts) {
		this.parent = opts.parent;
	}

	calendar_options() {
		return Object.assign(super.calendar_options(), {
			eventClassNames: "item-booking-calendar",
		})
	}

	getEvents(parameters) {
		return frappe.call("erpnext.venue.doctype.item_booking.item_booking.get_bookings_list_for_map", {
			start: this.format_ymd(parameters.start),
			end: this.format_ymd(parameters.end),
		}).then(result => {
			return result.message || [];
		})
	}

	onEventClick(event) {
		const props = event.event.extendedProps;
		if (props?.status === "In cart") {
			window.location.href = "/cart";
			return;
		}

		if (
			this.parent.can_cancel == "0" ||
			frappe.datetime.get_diff(event.event.start, frappe.datetime.add_minutes(frappe.datetime.nowdate(), parseInt(this.parent.cancellation_delay))) <= 0
		) {
			return
		}

		frappe.confirm(__('Do you want to cancel this booking ?'), () => {
			frappe.call({
				method: "erpnext.venue.doctype.item_booking.item_booking.cancel_appointment",
				args: {
					id: event.event.id
				}
			}).then(() => {
				this.refetchEvents();
			})
		});
	}

	getSelectAllow(selectInfo) {
		return moment().diff(selectInfo.start) <= 0
	}

	getValidRange() {
		return { start: moment().format("YYYY-MM-DD") }
	}
}
