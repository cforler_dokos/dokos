// Copyright (c) 2023, Dokos SAS and Contributors
// See license.txt

import EventEmitterMixin from 'frappe/public/js/frappe/event_emitter';

import "frappe/public/js/frappe/ui/web_calendar";

frappe.provide("erpnext.booking_section");
frappe.provide("erpnext.booking_section_update")

erpnext.booking_section_update = {}

erpnext.booking_section = class BookingDialog {
	constructor(opts) {
		Object.assign(this, opts);
		Object.assign(erpnext.booking_section_update, EventEmitterMixin);

		this.skip_cart = this.skip_cart == "True";
		this.read_only = frappe.session.user === "Guest";
		this.wrapper = document.getElementsByClassName(this.parentId)[0];

		/** @type {BookingCalendar?} */ this.booking_calendar = null;
		this.build_calendar();
	}

	get_can_cancel_before() {
		if (!this.can_cancel) return null;
		const delay_minutes = this.cancellation_delay / 60;
		return frappe.datetime.add_minutes(frappe.datetime.nowdate(), delay_minutes);
	}

	build_calendar() {
		this.booking_calendar = new BookingCalendar({
			wrapper: this.wrapper,
			booking_dialog: this,
		})

		erpnext.booking_section_update.on("update_calendar", r => {
			this.uom = r;
			this.booking_calendar.booking_selector?.empty();
			this.booking_calendar?.refetchEvents();
		})
	}

	destroy_calendar() {
		this.booking_calendar.destroy();
	}
}

class BookingCalendar extends frappe.ui.BaseWebCalendar {
	init() {
		this.slots = [];
		/** @type {erpnext.booking_section?} */
		this.booking_dialog = this.options.booking_dialog;
	}

	async getEvents(parameters) {
		const result = await frappe.call("erpnext.templates.pages.cart.get_availabilities_for_cart", {
			start: this.format_ymd(parameters.start),
			end: this.format_ymd(parameters.end),
			item: this.booking_dialog.item,
			uom: this.booking_dialog.uom
		});
		const slots = result.message

		// Group slots by status
		const events = this.aggregateSlots(slots);

		this.slots = slots;

		const date = this.booking_dialog.date
		if (date && !this.booking_selector) {
			this._make_booking_selector({ date });
		} else {
			this.booking_selector?.make();
		}

		return events || this.slots;
	}

	get_header_toolbar() {
		return {
			left: 'dayGridMonth,timeGridWeek,listDay',
			center: 'prev,title,next',
			right: 'today',
		}
	}

	_make_booking_selector(date_info) {
		this.booking_selector = new BookingSelector({
			booking_calendar: this,
			booking_dialog: this.booking_dialog,
			date_info: date_info,
		});
	}

	onDateClick(info) {
		this._make_booking_selector(info);
	}

	onDatesSet(info) {
		this.booking_selector?.empty();
	}

	onEventClick(info) {
		this._make_booking_selector({ date: info.event.start });

		if (info.event.extendedProps.slot) {
			this.booking_selector?.select_slot(info.event.extendedProps.slot);
		}
	}

	get_initial_date() {
		const queryParamStartDate = new URLSearchParams(window.location.search).get("start_date");
		if (this.booking_dialog.date) {
			return this.format_ymd(this.booking_dialog.date);
		} else if (queryParamStartDate) {
			return queryParamStartDate;
		} else {
			return moment().add(1,'d').format("YYYY-MM-DD");
		}
	}

	calendar_options() {
		return Object.assign(super.calendar_options(), {
			initialView: "dayGridMonth",
			eventOrder: "start,status",
			noEventsContent: __("No slot available"),
			eventClassNames(arg) {
				return ["booking-calendar", arg.event.extendedProps.status || ""]
			},
			eventDidMount: (info) => {
				decorateSlot(info);
			},
		});
	}

	set_loading_state(state) {
		super.set_loading_state(state);
		const element = document.querySelector(".item-booking-section");
		if (state === "loading") {
			element?.setAttribute("loading", "loading");
		} else if (state === "longloading") {
			element?.setAttribute("loading", "loading");
		} else if (state === "done") {
			element?.removeAttribute("loading");
		}
	}

	getSelectAllow(selectInfo) {
		return moment().diff(selectInfo.start) <= 0
	}

	getValidRange() {
		return { start: moment().format("YYYY-MM-DD") }
	}

	aggregateSlots(slots) {
		const aggregatedSlots = {};
		const counterStatusPerDay = {};
		for (const slot of slots) {
			const date = momentjs(slot.start).format("YYYY-MM-DD");
			const status = slot.status;

			if (!(date in aggregatedSlots)) {
				aggregatedSlots[date] = [];
				counterStatusPerDay[date] = {};
			}
			aggregatedSlots[date].push(slot);

			if (!(status in counterStatusPerDay[date])) {
				counterStatusPerDay[date][status] = 0;
			}
			counterStatusPerDay[date][status]++;
		}

		const mapStatusToColor = {
			"available": "var(--green-600)",
			"selected": "var(--orange-500)",
			"confirmed": "var(--blue-400)",
		};
		const getSingleTitle = (slot) => {
			const s = momentjs(slot.start).format("HH:mm");
			const e = momentjs(slot.end).format("HH:mm");
			let title = s + "–" + e;
			if (slot.status === "selected") {
				title += " · " + __("In Cart");
			} else if (slot.status === "confirmed") {
				title += " · " + __("Confirmed");
			}
			return title;
		};
		const events = Object.entries(aggregatedSlots)
			.sort(([a], [b]) => a.localeCompare(b))
			.flatMap(([key, slots]) => {
				const date = momentjs(slots[0].start).format("YYYY-MM-DD");

				let status = "available";
				if (counterStatusPerDay[date].selected > 0) {
					status = "selected";
				}

				const color = mapStatusToColor[status];
				const title = [
					counterStatusPerDay[date].available,
					counterStatusPerDay[date].selected,
				].filter(x => x > 0).join(" + ") || "0";

				return [
					{
						// Badge
						start: date,
						end: date,
						title,
						textColor: color,
						borderColor: color,
						backgroundColor: "white",
						extendedProps: { status },
						allDay: 1,
						className: "booking-slot-badge",
					},
					{
						// Background
						start: date,
						end: date,
						color,
						display: "background",
						allDay: 1,
						className: "booking-slot-background",
					},
					...slots.map(slot => ({
						...slot,
						color: mapStatusToColor[slot.status],
						display: "block",
						allDay: false,
						className: "booking-slot-single",
						extendedProps: { slot },
						title: getSingleTitle(slot),
					})),
				];
			});
		return events;
	}
}

class BookingSelector {
	constructor(opts) {
		/** @type {erpnext.booking_section} */ this.booking_dialog = null;
		/** @type {BookingCalendar} */ this.booking_calendar = null;
		Object.assign(this, opts);
		this.credits = 0.0;
		this.make()
	}

	async make() {
		this.slots = this.getSelectableSlots();
		await this.refreshCredits();
		this.build();
		this.render();
	}

	async refreshCredits() {
		try {
			const res = await frappe.call({
				method: "erpnext.venue.doctype.booking_credit.booking_credit.get_booking_credits_by_item",
				args: {
					item: this.booking_dialog.item,
					uom: this.booking_dialog.uom
				}
			});
			if (res.message) {
				this.credits = res.message;
			}
		} catch (e) {
			console.error(e);
		}
	}

	getSelectableSlots() {
		const date = frappe.datetime.get_date(this.date_info.date);
		return this.booking_calendar.slots.filter(s => frappe.datetime.get_date(s.start) == date)
	}

	getSlotById(id) {
		return this.slots.find((slot) => slot.id === id);
	}

	build(slots = this.slots) {
		const locale = this.booking_calendar.locale;

		/** @type {Date | null} */
		const date = this.date_info.date;

		const format_date = (date, fmt = "LT") => {
			return momentjs(date).locale(locale).format(fmt)
		}

		const title_map = {
			"available": __("Available"),
			"selected": __("In Cart"),
			"confirmed": __("Confirmed"),
		};

		const slots_div = slots.length ? slots.sort((a,b) => new Date(a.start) - new Date(b.start)).map(s => {
			let left = "";
			let right = "";
			if (s.status === "confirmed" || s.status === "selected") {
				const icon_name = s.status === "confirmed" ? "check" : "assets";
				const icon = frappe.utils.icon(icon_name, "md");
				left += `<div aria-hidden="true" class="position-absolute" style="left:0;--icon-stroke:white;">${icon}</div>`
			}
			if (s.status === "selected" && s.number > 0) {
				right += `<div aria-hidden="true" style="right:0;height:1.5em;width:1.5em;" class="position-absolute bg-light text-primary rounded-circle shadow-sm">${s.number}</div>`;
			}

			const title = title_map[s.status] ?? "";

			return `<div class="timeslot-options mb-4 px-4" data-slot-id="${s.id}" title="${title}">
				<button class="btn btn-outline-secondary ${s.status}" type="button">
					<div class="d-flex justify-content-center position-relative">
						${left}
						<div class="mx-auto">
							${format_date(s.start, 'LT')} - ${format_date(s.end, 'LT')}
						</div>
						${right}
					</div>
				</button>
			</div>`
		}): [];

		this.$content = $(`<div>
			<h2 class="timeslot-options-title text-muted mb-4">${date ? format_date(date, 'LL') : ""}</h2>
			${slots_div.join('')}
		</div>`)

		this.$content.on("click", ".timeslot-options", this.onSlotClick.bind(this));
	}

	empty() {
		$(".booking-selector").empty()
	}

	render() {
		this.empty()
		$(".booking-selector").append(this.$content)
	}

	onSlotClick(domEvent) {
		/** @type {HTMLElement} */
		const element = domEvent.currentTarget;
		const selected_slot = this.getSlotById(element.getAttribute("data-slot-id"));
		this.select_slot(selected_slot)
	}

	select_slot(slot) {
		if (!slot) return;

		if (frappe.session.user == "Guest") {
			return window.location = `/login?redirect-to=${window.location.pathname}?date=${slot.start}`
		}

		if (slot.status == "selected") {
			this.remove_booked_slot(slot.id)
		} else if (slot.status == "confirmed") {
			const max_cancel_date = this.booking_dialog.get_can_cancel_before();
			const slot_start = frappe.datetime.add_minutes(slot.start, 0);
			if (!max_cancel_date || slot_start < max_cancel_date) {
				// window.location = "/bookings"
				return;
			}

			frappe.confirm(__("Do you want to cancel this booking ?"), () => {
				frappe.call({
					method: "erpnext.venue.doctype.item_booking.item_booking.cancel_appointment",
					args: { id: slot.id }
				}).then(() => {
					this.booking.refetchEvents();
				})
			});
		} else {
			if (this.credits > 0) {
				this.showUseYourCreditsDialog(slot);
			} else {
				this.book_new_slot({ slot })
			}
		}
	}

	showUseYourCreditsDialog(slot) {
		const dialog = new frappe.ui.Dialog({
			title: __("Use your credits"),
			size: "large",
			fields: [
				{
					"fieldtype": "HTML",
					"fieldname": "content",
				}
			],
			primary_action_label: __("Use my credits"),
			primary_action: () => {
				this.book_new_slot({ slot, with_credits: true });
				dialog.hide();
			},
			secondary_action_label: __("Do not use my credits"),
			secondary_action: () => {
				this.book_new_slot({ slot });
				dialog.hide();
			}
		});
		const credits_str = `${this.credits} ${this.credits > 1 ? __("credits") : __("credit")}`;
		dialog.fields_dict.content.$wrapper.html(`
			<div>
				<h6>${__("You have {0} available for this resource.", [credits_str])}</h6>
				<h6>${__("Do you want to use your credits for this booking ?")}</h6>
			</div>
		`);
		dialog.show();
	}

	book_new_slot({ slot, with_credits }) {
		// if (with_credits) this.credits = Math.max(0, this.credits - 1); // Simulate credit usage

		frappe.call("erpnext.venue.doctype.item_booking.item_booking.book_new_slot", {
			start: moment.utc(slot.start).format("YYYY-MM-DD HH:mm:SS"),
			end: moment.utc(slot.end).format("YYYY-MM-DD HH:mm:SS"),
			item: this.booking_dialog.item,
			uom: this.booking_dialog.uom,
			user: frappe.session.user,
			status: this.booking_dialog.skip_cart ? "Confirmed": null,
			with_credits: with_credits,
		}).then(r => {
			if (!this.booking_dialog.skip_cart) {
				this.update_cart(r.message.name, 1)
			} else {
				this.booking_calendar?.refetchEvents();
			}
		})
	}

	remove_booked_slot(booking_id) {
		if (!this.booking_dialog.skip_cart) {
			this.update_cart(booking_id, 0)
		} else {
			frappe.call("erpnext.venue.doctype.item_booking.item_booking.remove_booked_slot", {
				name: booking_id
			}).then(r => {
				this.booking_calendar?.refetchEvents();
			})
		}
	}

	update_cart(booking, qty) {
		erpnext.e_commerce.shopping_cart.shopping_cart_update({
			item_code: this.booking_dialog.item,
			qty: qty,
			uom: this.booking_dialog.uom,
			booking: booking,
			cart_dropdown: true,
		}).then(() => {
			this.booking_calendar?.refetchEvents();
		})
	}
}

function decorateSlot(info) {
	const classNames = info.event.classNames ?? [];
	const view = info.view.type;

	let visible = true;
	const isSingleSlot = classNames.includes("booking-slot-single");

	if (isSingleSlot && view === "dayGridMonth") {
		visible = false;
	} else if (!isSingleSlot && ["timeGridWeek", "listDay"].includes(view)) {
		visible = false;
	}

	if (visible) {
		delete info.el.style.display;
	} else {
		info.el.style.display = "none";
	}

	// Create the icon
	if (isSingleSlot && visible) {
		const status = info.event.extendedProps.status;
		const iconName = {
			"selected": "assets",
			"confirmed": "tick",
		}[status] ?? "";

		if (iconName) {
			info.el.querySelector(".booking-slot-icon")?.remove();

			const icon = document.createElement("span");
			icon.classList.add("booking-slot-icon");

			if (view.includes("list")) {
				/** @type {HTMLElement} */
				icon.innerHTML = frappe.utils.icon(iconName, "md");
				const dot = info.el.querySelector(".fc-list-event-dot");
				dot.style.display = "none";
				dot.after(icon);
			} else {
				icon.innerHTML = frappe.utils.icon(iconName, "lg");
				const titleEl = info.el.querySelector(".fc-event-title");
				titleEl.innerHTML = "";
				info.el.append(icon);
			}
		}
	}
}