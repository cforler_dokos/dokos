# Copyright (c) 2020, Dokos SAS and Contributors
# License: GNU General Public License v3. See license.txt

import frappe
from frappe import _
from frappe.utils import cint, get_url

from erpnext.e_commerce.shopping_cart.cart import get_shopping_cart_settings


def get_context(context):
	context.no_cache = 1
	context.show_sidebar = True
	context.breadcrumbs = True

	if not cint(get_shopping_cart_settings().enabled) or frappe.session.user == "Guest":
		frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

	try:
		context.doc = frappe.get_doc(frappe.form_dict.doctype, frappe.form_dict.name)
		context.title = _("Subscription {0}").format(frappe.form_dict.name)
	except Exception:
		frappe.local.flags.redirect_location = "/subscriptions"
		raise frappe.Redirect

	context.next_invoice_date = (
		context.doc.get_next_invoice_date() if hasattr(context.doc, "get_next_invoice_date") else None
	)

	context.payment_requests = get_open_payment_requests_for_subscription(frappe.form_dict.name)
	context.customer = context.doc.get_customer_details(ignore_permissions=True)
	context.billing_contact = {
		"name": " ".join(frappe.db.get_value("Contact", context.doc, ["first_name", "last_name"])),
		"email": context.doc.contact_email,
	}


def get_open_payment_requests_for_subscription(subscription):
	output = []
	orders = frappe.get_all("Sales Order", filters={"subscription": subscription}, pluck="name")
	invoices = frappe.get_all("Sales Invoice", filters={"subscription": subscription}, pluck="name")
	payment_requests = frappe.get_all(
		"Payment Request",
		filters={
			"docstatus": 1,
			"status": "Initiated",
			"reference_doctype": ("in", ("Sales Order", "Sales Invoice")),
			"reference_name": ("in", orders + invoices),
		},
		fields=["name", "payment_key", "grand_total", "reference_doctype", "reference_name", "currency"],
		order_by="transaction_date desc",
		distinct=True,
	)

	for payment_request in payment_requests:
		if payment_request.reference_doctype == "Sales Invoice":
			invoice = frappe.db.get_value(
				payment_request.reference_doctype,
				payment_request.reference_name,
				("outstanding_amount", "docstatus", "from_date", "to_date"),
				as_dict=True,
			)
			payment_request["from_date"] = invoice.from_date
			payment_request["to_date"] = invoice.to_date
			if invoice.docstatus != 1 or not invoice.outstanding_amount:
				frappe.db.set_value("Payment Request", payment_request.name, "status", "Cancelled")
				continue

		elif payment_request.reference_doctype == "Sales Order":
			order = frappe.db.get_value(
				payment_request.reference_doctype,
				payment_request.reference_name,
				("per_billed", "docstatus", "status", "from_date", "to_date"),
				as_dict=True,
			)
			payment_request["from_date"] = order.from_date
			payment_request["to_date"] = order.to_date
			if order.docstatus != 1 or order.per_billed >= 100.0 or order.status in ("Completed", "Closed"):
				frappe.db.set_value("Payment Request", payment_request.name, "status", "Cancelled")
				continue

		payment_request["payment_link"] = get_url(
			"/payments?link={0}".format(payment_request.payment_key)
		)
		output.append(payment_request)

	return output


@frappe.whitelist()
def add_plan(subscription, plan):
	subscription = frappe.get_doc("Subscription", subscription)
	subscription.add_plan(plan)
	return subscription.save(ignore_permissions=True)


@frappe.whitelist()
def remove_subscription_line(subscription, line):
	subscription = frappe.get_doc("Subscription", subscription)
	subscription.remove_plan(line)
	return subscription.save(ignore_permissions=True)


@frappe.whitelist()
def cancel_subscription(subscription):
	subscription = frappe.get_doc("Subscription", subscription)
	if subscription.current_invoice_end:
		return subscription.cancel_subscription(ignore_permissions=True)
	else:
		return frappe.delete_doc("Subscription", subscription.name, ignore_permissions=True)
