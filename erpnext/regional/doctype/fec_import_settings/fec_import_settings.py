# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class FECImportSettings(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.regional.doctype.fec_accounting_journal_mapping.fec_accounting_journal_mapping import (
			FECAccountingJournalMapping,
		)

		accounting_journal_mapping: DF.Table[FECAccountingJournalMapping]
		company: DF.Link
		create_purchase_invoices: DF.Check
		create_sales_invoices: DF.Check
		customer_group: DF.Link
		purchase_item: DF.Link | None
		sales_item: DF.Link | None
		submit_journal_entries: DF.Check
		submit_purchase_invoices: DF.Check
		submit_sales_invoices: DF.Check
		supplier_group: DF.Link
		territory: DF.Link
	# end: auto-generated types
	pass
