// Copyright (c) 2023, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on('FEC Import', {
	setup(frm) {
		frm.set_query("import_settings", function(doc) {
			return {
				filters: {
					"company": doc.company
				}
			}
		})

		frappe.realtime.on('fec_doc_update', data => {
			if (data.fec_import !== frm.doc.name) return;
			const message = __('{0} of {1} {2}', [data.current, data.total, data.type]);
			const percent = Math.floor((data.current * 100) / data.total);
			const title = __("{0} creation", [data.type])
			frm.dashboard.show_progress(title, percent, message);
		})
	},
	refresh(frm) {
		frm.get_field("fec_file").df.options = {
			restrictions: {
				allowed_file_types: [".txt", ".csv"],
			},
		};

		frm.trigger("add_description");
		frm.trigger("add_actions");
	},

	add_actions(frm) {
		if (frm.doc.fec_file) {
			frm.page.set_primary_action(__("Import FEC"), function() {
				frappe.show_alert({
					message: __("Import started"),
					indicator: "orange"
				})
				frappe.call({
					method: "upload_fec",
					doc: frm.doc,
				}).then(r => {
					frappe.show_alert({
						message: __("Import finished. Please check the imported documents."),
						indicator: "green"
					})
				})
			});

			frm.add_custom_button(__("Create Accounts"), function() {
				frappe.show_alert({
					message: __("Accounts creation started"),
					indicator: "orange"
				})
				frappe.call({
					method: "create_accounts",
					doc: frm.doc,
				}).then(r => {
					frappe.show_alert({
						message: __("Accounts created.<br>Please setup a correct account type for each."),
						indicator: "green"
					})
				})
			}, __("Actions"));

			frm.add_custom_button(__("Create Journals"), function() {
				frappe.show_alert({
					message: __("Journals creation started"),
					indicator: "orange"
				})
				frappe.call({
					method: "create_journals",
					doc: frm.doc,
				}).then(r => {
					frappe.show_alert({
						message: __("Accounting journals created.<br>Please setup a correct journal type for each."),
						indicator: "green"
					})
				})
			}, __("Actions"));

			frm.add_custom_button(__("View Accounts"), function() {
				frappe.set_route("Tree", "Account")
			}, __("View"));

			frm.add_custom_button(__("View Journals"), function() {
				frappe.set_route("List", "Accounting Journal")
			}, __("View"));
		}

		frappe.model.with_doctype("FEC Import Document", () => {
			frappe.db.get_list("FEC Import Document", {
				filters: {
					fec_import: frm.doc.name,
				}
			}).then(r => {
				frm.add_custom_button(__("Auto reconcile open entries"), function() {
					frappe.show_alert({
						message: __("Reconciliation started"),
						indicator: "orange"
					})
					frappe.call({
						method: "auto_reconcile_entries",
						doc: frm.doc,
					}).then(r => {
						frappe.show_alert({
							message: __("Reconciliation completed."),
							indicator: "green"
						})
					})
				}, __("Actions"));
			})
		})
	},

	add_description(frm) {
		let description = __("1. Import your FEC file")
		if (!frm.is_new()) {
			description += "</br>"
			description += __("2. Import your accounts.")
			description += "</br>"
			description += "<i>"
			description += __("This operation will not erase your existing accounts. Dokos will try to append them to their corresponding parents in the tree.")
			description += "</i>"
			description += "</br>"
			description += __("3. Configure your accounts properly by adding the correct account type, especially for receivable and payable accounts (Classe 4).")
			description += "</br>"
			description += __("4. Import your accounting journals.")
			description += "</br>"
			description += "<i>"
			description += __("This operation will not erase your existing accounting journals.")
			description += "</i>"

			description += "</br>"
			description += __("5. Configure your journals properly, especially the Bank and Cash journals.")
		}

		description += "</br>"

		frm.get_field("description").$wrapper.html(description)
		frm.get_field("description").$wrapper.addClass("mb-4")
	}
});
