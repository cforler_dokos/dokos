# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import codecs
import datetime
import hashlib
from collections import defaultdict

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import cint, flt, get_year_start, getdate
from frappe.utils.csvutils import read_csv_content


class FECImport(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		company: DF.Link | None
		fec_file: DF.Attach | None
		from_date: DF.Date | None
		import_journal: DF.Link | None
		import_settings: DF.Link | None
		naming_series: DF.Literal["FEC-.YYYY.-.#####"]
		to_date: DF.Date | None
	# end: auto-generated types

	def validate(self):
		company_and_dates = self.get_company_and_dates()
		if not self.company:
			self.company = company_and_dates.company

		if not self.from_date:
			self.from_date = company_and_dates.from_date

		if not self.to_date:
			self.to_date = company_and_dates.to_date

		if self.company and not self.import_settings:
			if (
				settings := frappe.get_all(
					"FEC Import Settings", filters={"company": self.company}, pluck="name"
				)
			) and len(settings) == 1:
				self.import_settings = settings[0]

	@frappe.whitelist()
	def get_company_and_dates(self):
		company_and_dates = frappe._dict(company=None, from_date=None, to_date=None)

		if self.fec_file:
			file_name = frappe.db.get_value(
				"File",
				{
					"file_url": self.fec_file,
					"attached_to_doctype": self.doctype,
					"attached_to_name": self.name,
				},
				"file_name",
			)
			try:
				if closing_date := file_name.split("FEC")[1]:
					closing_date = closing_date.split(".txt")[0]
					company_and_dates["to_date"] = datetime.datetime.strptime(closing_date, "%Y%m%d").strftime(
						"%Y-%m-%d"
					)
					company_and_dates["from_date"] = get_year_start(closing_date, as_str=True)

				if siren := file_name.split("FEC")[0]:
					company_and_dates["company"] = frappe.db.get_value("Company", dict(siren_number=siren))
			except Exception:
				company_and_dates

		return company_and_dates

	@frappe.whitelist()
	def upload_fec(self):
		if not self.company:
			frappe.throw(_("Please select a company"))

		if not self.import_settings:
			frappe.throw(_("Please select an import settings document"))

		data = self.get_data()

		try:
			import_in_progress = FECImportDocumentCreator(settings=self, data=data)
			import_in_progress.import_data()
		except Exception:
			print(frappe.get_traceback())

	def get_data(self):
		fileid = frappe.db.get_value(
			"File",
			{"file_url": self.fec_file, "attached_to_doctype": self.doctype, "attached_to_name": self.name},
		)

		_file = frappe.get_doc("File", fileid)
		fcontent = _file.get_content()
		rows = read_csv_content(fcontent, delimiter="\t")

		header = rows[0]
		data = rows[1:]

		output = list()
		for d in data:
			row = frappe._dict()
			for count, head in enumerate(header):
				if head:
					if head.__contains__(codecs.BOM_UTF8.decode("utf-8")):
						# A Byte Order Mark is present
						head = head.strip(codecs.BOM_UTF8.decode("utf-8"))
					row[head] = d[count] or ""

			output.append(row)

		return output

	@frappe.whitelist()
	def create_journals(self):
		journals = {l["JournalCode"]: l["JournalLib"] for l in self.get_data()}
		bank_journals = list(
			{l["JournalCode"] for l in self.get_data() if l["CompteNum"].startswith("512")}
		)
		cash_journals = list(
			{l["JournalCode"] for l in self.get_data() if l["CompteNum"].startswith("53")}
		)
		sales_journals = list(
			{l["JournalCode"] for l in self.get_data() if l["CompteNum"].startswith("7")}
		)
		purchase_journals = list(
			{l["JournalCode"] for l in self.get_data() if l["CompteNum"].startswith("6")}
		)

		for index, journal in enumerate(journals):
			frappe.publish_realtime(
				"fec_doc_update",
				{
					"fec_import": self.name,
					"current": index + 1,
					"total": len(journals),
					"type": _("accounting journals created"),
				},
			)
			journal_type = "Miscellaneous"
			account = None

			if journal in bank_journals:
				journal_type = "Bank"
				account_number = list(
					{
						l["CompteNum"]
						for l in self.get_data()
						if l["JournalCode"] == journal and l["CompteNum"].startswith("512")
					}
				)[0]
				account = frappe.get_value("Account", dict(account_number=account_number))
			elif journal in cash_journals:
				journal_type = "Cash"
				account_number = list(
					{
						l["CompteNum"]
						for l in self.get_data()
						if l["JournalCode"] == journal and l["CompteNum"].startswith("53")
					}
				)[0]
				account = frappe.get_value("Account", dict(account_number=account_number))
			elif journal in sales_journals:
				journal_type = "Sales"
			elif journal in purchase_journals:
				journal_type = "Purchase"

			doc = frappe.new_doc("Accounting Journal")
			doc.company = self.company
			doc.journal_code = journal
			doc.journal_name = journals[journal]
			doc.type = journal_type
			doc.account = account
			doc.insert(ignore_if_duplicate=True)

	@frappe.whitelist()
	def create_accounts(self):
		accounts = {l["CompteNum"]: l["CompteLib"] for l in self.get_data()}
		account_groups = frappe.get_all(
			"Account",
			filters={
				"disabled": 0,
				"is_group": 1,
				"account_number": ("is", "set"),
				"company": self.company,
				"do_not_show_account_number": 0,
			},
			fields=["name", "parent_account", "account_number"],
		)
		accounts_with_number_list = [x.name for x in account_groups]

		account_groups_with_numbers = {}
		for group in account_groups:
			if not group.parent_account in accounts_with_number_list:
				account_groups_with_numbers.update({group.account_number[:-1]: group.parent_account})
			account_groups_with_numbers.update({group.account_number: group.name})

		for index, account in enumerate(accounts):
			frappe.publish_realtime(
				"fec_doc_update",
				{
					"fec_import": self.name,
					"current": index + 1,
					"total": len(accounts),
					"type": _("accounts created"),
				},
			)
			if not frappe.db.exists("Account", dict(account_number=account)):
				doc = frappe.new_doc("Account")
				doc.company = self.company
				doc.account_name = accounts[account]
				doc.account_number = account
				doc.parent_account = self.get_parent_account(account_groups_with_numbers, account)
				doc.account_type = self.get_account_type(account)
				doc.insert(ignore_if_duplicate=True)

	def get_parent_account(self, account_groups, account):
		if frappe.db.exists(
			"Account", dict(account_number=str(account)[:-1], disabled=0, company=self.company)
		) and account_groups.get(account[:-1]):
			return account_groups.get(account[:-1])

		account_numbers = [key for key, value in account_groups.items()]
		for idx, acc in enumerate(account):
			sub_number = account[:idx]
			if sub_number in account_numbers:
				continue
			elif account_groups.get(account[: idx - 1]):
				return account_groups[account[: idx - 1]]

		root_mapping = {
			"1": "Equity",
			"2": "Asset",
			"3": "Asset",
			"4": "Liability",
			"5": "Equity",
			"6": "Expense",
			"7": "Income",
		}

		return frappe.db.get_value(
			"Account",
			dict(
				disabled=0,
				root_type=root_mapping.get(str(account)[0]),
				company=self.company,
				parent_account=("is", "not set"),
			),
		)

	def get_account_type(self, account_number):
		if cint(account_number[:3]) in range(400, 409):
			return "Payable"

		elif cint(account_number[:3]) in range(410, 419):
			return "Receivable"

		elif cint(account_number[:3]) == 512:
			return "Bank"

		elif cint(account_number[:2]) == 53:
			return "Cash"

		elif cint(account_number[:2]) == 10:
			return "Equity"

	@frappe.whitelist()
	def auto_reconcile_entries(self):
		from erpnext.accounts.report.accounts_receivable.accounts_receivable import (
			ReceivablePayableReport,
		)

		for party_type in ["Customer", "Supplier"]:
			args = {
				"account_type": "Payable" if party_type == "Supplier" else "Receivable",
				"naming_by": ["Buying Settings", "supp_master_name"]
				if party_type == "Supplier"
				else ["Selling Settings", "cust_master_name"],
			}

			filters = {
				"company": self.company,
				"ageing_based_on": "Due Date",
				"range1": 30,
				"range2": 60,
				"range3": 90,
				"range4": 120,
			}

			result = ReceivablePayableReport(filters).run(args)
			parties = set(
				(d.get("party"), d.get("party_account")) for d in result[1] if d.get("party") != "Total"
			)

			for index, data in enumerate(parties):
				frappe.publish_realtime(
					"fec_doc_update",
					{
						"fec_import": self.name,
						"current": index + 1,
						"total": len(parties),
						"type": _("parties reconciled"),
					},
				)
				pr = self.create_payment_reconciliation(party_type, data[0], data[1])
				pr.get_unreconciled_entries()
				invoices = [x.as_dict() for x in pr.get("invoices")]
				payments = [x.as_dict() for x in pr.get("payments")]
				if invoices and payments:
					pr.allocate_entries(frappe._dict({"invoices": invoices, "payments": payments}))
					pr.reconcile()

	def create_payment_reconciliation(self, party_type, party, party_account):
		pr = frappe.new_doc("Payment Reconciliation")
		pr.company = self.company
		pr.party_type = party_type
		pr.party = party
		pr.receivable_payable_account = party_account
		return pr


class FECImportDocumentCreator:
	def __init__(self, settings, data):
		self.settings = settings
		if not self.settings.import_settings:
			frappe.throw(
				_("Please configure a FEC Import Settings document for company {0}").format(
					self.settings.company
				)
			)

		self.data = data

	def import_data(self):
		self.group_data()
		self.create_fec_import_documents()
		self.process_fec_import_documents()

	def group_data(self):
		self.grouped_data = defaultdict(lambda: defaultdict(list))
		accounting_journals = self.get_accounting_journals_mapping()

		for index, d in enumerate(self.data):
			frappe.publish_realtime(
				"fec_doc_update",
				{
					"fec_import": self.settings.name,
					"current": index + 1,
					"total": len(self.data),
					"type": _("line processed"),
				},
			)
			if not self.is_within_date_range(d):
				continue

			if (
				self.settings.import_journal
				and not accounting_journals.get(d.get("JournalCode")) == self.settings.import_journal
			):
				continue

			self.grouped_data[d["EcritureDate"]][
				f'{d["JournalCode"]} | {d["EcritureNum"] or d["PieceRef"]}'
			].append(frappe._dict(d))

	@staticmethod
	def parse_credit_debit(d):
		d["Debit"] = flt(d["Debit"].replace(",", "."))
		d["Credit"] = flt(d["Credit"].replace(",", "."))
		d["Montantdevise"] = flt(d["Montantdevise"].replace(",", "."))

	def create_fec_import_documents(self):
		current_index = 0
		total_elements = sum(len(self.grouped_data[date]) for date in self.grouped_data)
		for date in self.grouped_data:
			for piece in self.grouped_data[date]:
				current_index += 1
				frappe.publish_realtime(
					"fec_doc_update",
					{
						"fec_import": self.settings.name,
						"current": current_index,
						"total": total_elements,
						"type": _("voucher creation initiated"),
					},
				)
				iter_next = False
				doc = frappe.new_doc("FEC Import Document")
				doc.fec_import = self.settings.name
				doc.settings = self.settings.import_settings
				doc.gl_entries_date = datetime.datetime.strptime(date, "%Y%m%d").strftime("%Y-%m-%d")
				doc.gl_entry_reference = piece

				for line in self.grouped_data[date][piece]:
					concatenated_data = "".join([value for key, value in line.items()])
					self.parse_credit_debit(line)
					row = {frappe.scrub(key): value for key, value in line.items()}
					row["hashed_data"] = hash_line(concatenated_data)

					if frappe.db.exists("FEC Import Line", dict(hashed_data=row["hashed_data"])):
						iter_next = True
						break

					doc.append("gl_entries", row)

				if iter_next:
					continue

				doc.insert()

	def process_fec_import_documents(self):
		groups = {"Transaction": [], "Payment": [], "Miscellaneous": []}

		for doc in frappe.get_all(
			"FEC Import Document",
			filters={"status": "Pending"},
			fields=["name", "import_type"],
			order_by="gl_entries_date",
		):
			groups[doc.import_type].append(doc.name)

		for group in ["Transaction", "Miscellaneous", "Payment"]:
			for d in groups[group]:
				frappe.get_doc("FEC Import Document", d).run_method("process_document_in_background")

	def is_within_date_range(self, line):
		posting_date = datetime.datetime.strptime(line.EcritureDate, "%Y%m%d").strftime("%Y-%m-%d")

		if self.settings.from_date and getdate(posting_date) < getdate(self.settings.from_date):
			return False

		if self.settings.to_date and getdate(posting_date) > getdate(self.settings.to_date):
			return False

		return True

	def get_accounting_journals_mapping(self):
		dokos_journals = {
			x.journal_code: x.name
			for x in frappe.get_all(
				"Accounting Journal", filters={"disabled": 0}, fields=["journal_code", "name"]
			)
		}

		company_settings = frappe.get_doc("FEC Import Settings", self.settings.import_settings)

		journals = {}
		mapped_journals = dokos_journals
		for mapping in company_settings.get("accounting_journal_mapping", []):
			for j in mapped_journals:
				if j == mapping.accounting_journal_in_dokos:
					journals[mapping.accounting_journal_in_fec] = mapped_journals[j]
					continue

				journals[j] = mapped_journals[j]

			mapped_journals = journals

		return journals or dokos_journals


def hash_line(data):
	sha = hashlib.sha256()
	sha.update(frappe.safe_encode(str(data)))
	return sha.hexdigest()
