import frappe
from frappe.tests.utils import FrappeTestCase
from frappe.utils import cstr, flt

from erpnext.assets.doctype.asset.test_asset import create_asset
from erpnext.assets.doctype.asset_depreciation_schedule.asset_depreciation_schedule import (
	get_depr_schedule,
)
from erpnext.stock.doctype.purchase_receipt.test_purchase_receipt import make_purchase_receipt


class TestAssetForFrance(FrappeTestCase):
	@classmethod
	def setUpClass(cls):
		company = "_Test French Company"
		create_company(**dict(company_name=company, country="France", currency="EUR"))
		frappe.db.set_single_value("Accounts Settings", "mandatory_accounting_journal", 0)
		set_depreciation_settings_in_company(company)
		make_purchase_receipt(
			item_code="Macbook Pro",
			qty=1,
			rate=100000.0,
			location="Test Location",
			company=company,
			warehouse="Finished Goods - _FC",
		)
		frappe.flags.country = "France"

	@classmethod
	def tearDownClass(cls):
		frappe.flags.country = None

	def test_schedule_for_prorated_straight_line_method(self):
		asset = create_asset(
			calculate_depreciation=1,
			available_for_use_date="2030-01-30",
			purchase_date="2030-01-30",
			depreciation_method="Straight Line",
			expected_value_after_useful_life=10000,
			depreciation_start_date="2030-12-31",
			total_number_of_depreciations=3,
			frequency_of_depreciation=12,
			company="_Test French Company",
		)

		expected_schedules = [
			["2030-12-31", 27583.33, 27583.33],
			["2031-12-31", 30000.0, 57583.33],
			["2032-12-31", 30000.0, 87583.33],
			["2033-01-30", 2416.67, 90000.0],
		]

		schedules = [
			[
				cstr(d.schedule_date),
				flt(d.depreciation_amount, 2),
				flt(d.accumulated_depreciation_amount, 2),
			]
			for d in get_depr_schedule(asset.name, "Draft")
		]

		self.assertEqual(schedules, expected_schedules)


def create_company(**args):
	args = frappe._dict(args)
	company = frappe.get_doc(
		{
			"doctype": "Company",
			"company_name": args.company_name,
			"country": args.country,
			"default_currency": args.currency,
		}
	)
	company.insert(ignore_if_duplicate=True)
	return company.name


def set_depreciation_settings_in_company(company):
	company = frappe.get_doc("Company", company)
	company.accumulated_depreciation_account = "Accumulated Depreciation - " + company.abbr
	company.depreciation_expense_account = "Depreciation - " + company.abbr
	company.disposal_account = "Gain/Loss on Asset Disposal - " + company.abbr
	company.depreciation_cost_center = "Main - " + company.abbr
	company.save()

	# Enable booking asset depreciation entry automatically
	frappe.db.set_single_value("Accounts Settings", "book_asset_depreciation_entry_automatically", 1)
