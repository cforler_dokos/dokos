# Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


# import frappe
# from frappe import _
from frappe.model.document import Document


class SubscriptionPlan(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.accounts.doctype.subscription_plan_detail.subscription_plan_detail import (
			SubscriptionPlanDetail,
		)

		disabled: DF.Check
		plan_name: DF.Data
		portal_description: DF.TextEditor | None
		portal_image: DF.AttachImage | None
		subscription_plans_template: DF.Table[SubscriptionPlanDetail]
	# end: auto-generated types
	pass
