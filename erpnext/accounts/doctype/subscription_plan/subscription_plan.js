// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Subscription Plan', {
	setup(frm) {
		frm.set_query('item', 'subscription_plans_template', () => {
			return {
				query: "erpnext.controllers.queries.item_query",
				filters: {'is_sales_item': 1}
			}
		})
	}
});