from collections import defaultdict

import frappe


def execute():
	"""Update Mode of Payment Data from Payment Gateway"""
	pg_map = defaultdict(dict)
	for payment_gateway in frappe.get_all(
		"Payment Gateway", filters={"mode_of_payment": ("is", "set")}, fields=["*"]
	):
		pg_map[payment_gateway.name] = payment_gateway.mode_of_payment
		for field in ["fee_account", "tax_account", "cost_center", "icon"]:
			frappe.db.set_value(
				"Mode of Payment", payment_gateway.mode_of_payment, field, payment_gateway.get(field)
			)

	for subscription in frappe.get_all(
		"Subscription", filters={"payment_gateway": ("is", "set")}, fields=["payment_gateway", "name"]
	):
		frappe.db.set_value(
			"Subscription", subscription.name, "mode_of_payment", pg_map.get(subscription.payment_gateway)
		)
