# Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and Contributors
# See license.txt

from calendar import monthrange

import frappe
from frappe.tests.utils import FrappeTestCase
from frappe.utils import add_days, add_months, date_diff, getdate, nowdate, today

PLANS = [
	{
		"item": "_Test Non Stock Item",
		"qty": 1,
		"uom": "Unit",
		"fixed_rate": 150,
		"description": "Test Item",
		"price_determination": "Fixed rate",
	}
]


class TestSubscription(FrappeTestCase):
	@classmethod
	def setUpClass(cls) -> None:
		item = frappe.get_doc("Item", "_Test Non Stock Item")
		item.is_recurring_item = True

		for period in ["Daily", "Monthly"]:
			item.append("recurrence_periods", {"recurrence_period": period})
		item.save()

		frappe.get_doc(
			{
				"doctype": "Recurrence Period",
				"title": "Monthly EOP",
				"billing_interval": "Month",
				"billing_interval_count": 1,
				"generate_invoice_at_period_start": 0,
			}
		).insert(ignore_if_duplicate=True)

	def tearDown(self) -> None:
		frappe.flags.current_date = today()

	def test_period_update_with_trial(self):
		current_date = today()
		frappe.flags.current_date = today()

		subscription = create_subscription(
			**{
				"start": current_date,
				"recurrence_period": "Daily",
				"trial_period_end": add_days(current_date, 5),
				"generate_invoice_at_period_start": 1,
			}
		)
		subscription.process()

		for i in range(1, 10):
			frappe.flags.current_date = add_days(nowdate(), 1)
			subscription.process()
			self.assertEqual(getdate(subscription.trial_period_end), getdate(add_days(current_date, 5)))

			if i in range(1, 6):
				self.assertEqual(subscription.current_invoice_start, None)
				self.assertEqual(subscription.current_invoice_end, None)
				self.assertEqual(subscription.status, "Trial")
			else:
				self.assertEqual(subscription.current_invoice_start, getdate(add_days(current_date, i)))
				self.assertEqual(subscription.current_invoice_end, getdate(add_days(current_date, i)))
				self.assertEqual(subscription.status, "Payable")

		subscription.recurrence_period = "Monthly"
		subscription.save()
		invoicing_day = None
		current_invoice_end = subscription.current_invoice_end

		for i in range(1, date_diff(add_months(getdate(current_date), 2), current_date)):
			frappe.flags.current_date = add_days(nowdate(), 1)
			subscription.process()

			if current_invoice_end != subscription.current_invoice_end:
				month_max_no_of_days = monthrange(getdate(nowdate()).year, getdate(nowdate()).month)[1]
				if month_max_no_of_days > subscription.invoicing_day:
					invoicing_day = subscription.invoicing_day - 1
				else:
					invoicing_day = month_max_no_of_days - 1
				current_invoice_end = subscription.current_invoice_end

			self.assertEqual(subscription.trial_period_end, getdate(add_days(current_date, 5)))
			if getdate(frappe.flags.current_date) <= subscription.trial_period_end:
				self.assertEqual(subscription.current_invoice_start, None)
				self.assertEqual(subscription.current_invoice_end, None)
				self.assertEqual(subscription.status, "Trial")
			elif getdate(frappe.flags.current_date) <= add_months(subscription.trial_period_end, 1):
				self.assertEqual(
					subscription.current_invoice_start, add_days(subscription.trial_period_end, 5)
				)
				self.assertEqual(
					subscription.current_invoice_end,
					add_days(add_months(subscription.current_invoice_start, 1), -1),
				)
				self.assertEqual(subscription.status, "Payable")
			else:
				self.assertEqual(subscription.current_invoice_start.day, invoicing_day + 1)
				self.assertEqual(subscription.status, "Payable")

	def test_period_update_without_trial(self):
		current_date = today()
		frappe.flags.current_date = today()

		subscription = create_subscription(
			**{"start": current_date, "recurrence_period": "Daily", "generate_invoice_at_period_start": 1}
		)
		subscription.process()

		for i in range(1, 11):
			frappe.flags.current_date = add_days(nowdate(), 1)
			subscription.process()
			self.assertEqual(subscription.current_invoice_start, getdate(add_days(current_date, i)))
			self.assertEqual(subscription.current_invoice_end, getdate(add_days(current_date, i)))
			self.assertEqual(subscription.status, "Payable")

		subscription.recurrence_period = "Monthly"
		subscription.save()

		expected_start = getdate(add_days(getdate(current_date), 11))
		invoicing_day = None
		current_invoice_end = subscription.current_invoice_end

		for i in range(1, date_diff(add_months(getdate(current_date), 3), current_date)):
			frappe.flags.current_date = add_days(nowdate(), 1)
			subscription.process()

			if current_invoice_end != subscription.current_invoice_end:
				month_max_no_of_days = monthrange(getdate(nowdate()).year, getdate(nowdate()).month)[1]
				if month_max_no_of_days > subscription.invoicing_day:
					invoicing_day = subscription.invoicing_day - 1
				else:
					invoicing_day = month_max_no_of_days - 1
				current_invoice_end = subscription.current_invoice_end

			if getdate(frappe.flags.current_date) < add_months(expected_start, 1):
				self.assertEqual(subscription.current_invoice_start, expected_start)
				self.assertEqual(subscription.current_invoice_end, add_days(add_months(expected_start, 1), -1))
			elif getdate(frappe.flags.current_date) < add_months(expected_start, 2):
				self.assertEqual(subscription.current_invoice_start.day, invoicing_day + 1)
			self.assertEqual(subscription.status, "Payable")

	def test_invoice_generation(self):
		subscription = create_subscription(
			**{
				"start": today(),
				"recurrence_period": "Daily",
				"generate_invoice_at_period_start": 1,
				"generate_invoice_before_payment": 1,
			}
		)
		subscription.process()

		for i in range(1, 5):
			frappe.flags.current_date = add_days(nowdate(), i)
			subscription.process()

			invoices = frappe.get_all(
				"Sales Invoice",
				filters={
					"subscription": subscription.name,
					"from_date": add_days(subscription.current_invoice_start, -1),
					"to_date": add_days(subscription.current_invoice_end, -1),
				},
			)
			self.assertEqual(len(invoices), 1)

	def test_sales_order_generation(self):
		subscription = create_subscription(
			**{
				"start": today(),
				"recurrence_period": "Monthly",
				"generate_invoice_at_period_start": 1,
			}
		)

		self.assertEqual(subscription.status, "Pending")
		subscription.process()
		self.assertEqual(subscription.status, "Payable")

		sales_orders = frappe.get_all("Sales Order", filters={"subscription": subscription.name})
		self.assertEqual(len(sales_orders), 1)

		sales_invoice = frappe.get_all("Sales Invoice", filters={"subscription": subscription.name})
		self.assertEqual(len(sales_invoice), 0)

	def test_generate_subscription_from_sales_order(self):
		sales_order = frappe.new_doc("Sales Order")
		sales_order.customer = "_Test Customer"
		sales_order.company = "_Test Company"
		sales_order.currency = "INR"
		sales_order.recurrence_period = "Monthly"
		sales_order.transaction_date = nowdate()
		sales_order.delivery_date = add_days(nowdate(), 10)
		sales_order.append(
			"items",
			{
				"item_code": "_Test Non Stock Item",
				"qty": 1,
				"uom": "Unit",
				"description": "Test Item",
			},
		)
		sales_order.insert()
		sales_order.submit()

		sales_order.reload()
		self.assertTrue(sales_order.subscription)

	def test_payment_on_sales_order(self):
		subscription = create_subscription(
			**{
				"start": today(),
				"recurrence_period": "Monthly",
				"generate_invoice_at_period_start": 1,
			}
		)
		subscription.process()

		def check_sales_documents_unicity():
			sales_orders = frappe.get_all("Sales Order", filters={"subscription": subscription.name})
			self.assertEqual(len(sales_orders), 1)

			payment_requests = frappe.get_all(
				"Payment Request", filters={"subscription": subscription.name}
			)
			self.assertEqual(len(payment_requests), 1)

		def check_si_creation(number=0):
			sales_invoices = frappe.get_all("Sales Invoice", filters={"subscription": subscription.name})
			self.assertEqual(len(sales_invoices), number)

		check_sales_documents_unicity()
		subscription.process()

		check_sales_documents_unicity()
		check_si_creation(0)

		payment_request = frappe.get_doc("Payment Request", dict(subscription=subscription.name))

		payment_request.run_method("set_as_paid")
		payment_request.reload()
		self.assertEqual(payment_request.status, "Paid")

		check_si_creation(1)

		si_status = frappe.db.get_value("Sales Invoice", dict(subscription=subscription.name), "status")
		self.assertEqual(si_status, "Paid")

		so_status = frappe.db.get_value("Sales Order", dict(subscription=subscription.name), "status")
		self.assertEqual(so_status, "Completed")

	def test_end_of_period_invoicing(self):
		"""
		When invoicing occurs at the end of the period, the sales order should be generated at the beginning of the next period.
		"""
		subscription = create_subscription(**{"start": today(), "recurrence_period": "Monthly EOP"})
		subscription.process()
		from_date = subscription.current_invoice_start
		to_date = subscription.current_invoice_end

		sales_orders = frappe.get_all("Sales Order", filters={"subscription": subscription.name})
		self.assertEqual(len(sales_orders), 0)

		frappe.flags.current_date = add_days(subscription.current_invoice_end, 1)
		subscription.process()

		sales_orders = frappe.get_all("Sales Order", filters={"subscription": subscription.name})
		self.assertEqual(len(sales_orders), 1)
		sales_order = frappe.get_doc("Sales Order", sales_orders[0].name)
		self.assertEqual(sales_order.transaction_date, frappe.flags.current_date)
		self.assertEqual(sales_order.from_date, from_date)
		self.assertEqual(sales_order.to_date, to_date)


def create_subscription(*args, **kwargs):
	subscription = frappe.new_doc("Subscription")
	subscription.customer = "_Test Customer"
	subscription.company = "_Test Company"
	subscription.currency = "INR"

	subscription.start = kwargs.get("start") or today()

	subscription.trial_period_end = kwargs.get("trial_period_end")

	subscription.recurrence_period = kwargs.get("recurrence_period") or "Monthly"

	subscription.generate_invoice_before_payment = kwargs.get("generate_invoice_before_payment")
	subscription.generate_invoice_at_period_start = kwargs.get("generate_invoice_at_period_start")

	subscription.append(
		"plans",
		{
			"item": "_Test Non Stock Item",
			"qty": 1,
			"uom": "Unit",
			"fixed_rate": 150,
			"description": "Test Item",
			"price_determination": "Fixed rate",
		},
	)

	subscription.insert()

	return subscription
