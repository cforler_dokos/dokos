import frappe


def execute():
	frappe.reload_doc("accounts", "doctype", "Mode of Payment")
	fields = ["mode_of_payment", "fee_account", "tax_account", "cost_center", "icon"]
	fields += ["name"]

	for pg in frappe.get_all(
		"Payment Gateway", filters={"mode_of_payment": ("is", "set")}, fields=fields
	):
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "payment_gateway", pg.name)
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "fee_account", pg.fee_account)
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "tax_account", pg.tax_account)
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "cost_center", pg.cost_center)
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "icon", pg.icon)

	for cf in frappe.get_all(
		"Custom Field",
		filters={"is_system_generated": 1, "dt": "Payment Gateway", "fieldname": ("in", fields)},
	):
		frappe.delete_doc("Custom Field", cf.name)
