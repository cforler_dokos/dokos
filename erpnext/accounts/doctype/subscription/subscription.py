# Copyright (c) 2021, Dokos SAS and Contributors
# For license information, please see license.txt

import copy

import frappe
import numpy as np
from frappe import _
from frappe.model import default_fields, no_value_fields
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.utils.data import add_days, flt, getdate, global_date_format, nowdate

from erpnext.accounts.doctype.subscription.subscription_plans_manager import (
	SubscriptionPlansManager,
)
from erpnext.accounts.doctype.subscription.subscription_state_manager import (
	SubscriptionPeriod,
	SubscriptionStateManager,
)
from erpnext.accounts.doctype.subscription.subscription_transaction import (
	SubscriptionInvoiceGenerator,
	SubscriptionPaymentEntryGenerator,
	SubscriptionPaymentRequestGenerator,
	SubscriptionSalesOrderGenerator,
)
from erpnext.accounts.party import get_default_contact
from erpnext.venue.doctype.booking_credit.booking_credit import automatic_booking_credit_allocation

BILLING_STATUS = ["Billable", "Billing failed", "Cancelled and billable"]


class Subscription(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.accounts.doctype.subscription_plan_detail.subscription_plan_detail import (
			SubscriptionPlanDetail,
		)
		from erpnext.venue.doctype.booking_credit_allocation_rules.booking_credit_allocation_rules import (
			BookingCreditAllocationRules,
		)

		additional_discount_amount: DF.Currency
		additional_discount_percentage: DF.Percent
		apply_additional_discount: DF.Literal["", "Grand Total", "Net Total"]
		booking_credits_allocation: DF.Table[BookingCreditAllocationRules]
		cancellation_date: DF.Date | None
		company: DF.Link
		contact_email: DF.Data | None
		contact_person: DF.Link | None
		currency: DF.Link
		current_invoice_end: DF.Date | None
		current_invoice_start: DF.Date | None
		customer: DF.Link
		email_template: DF.Link | None
		generate_invoice_at_period_start: DF.Check
		generate_invoice_before_payment: DF.Check
		grand_total: DF.Currency
		invoicing_day: DF.Int
		mode_of_payment: DF.Link | None
		naming_series: DF.Literal["ACC-SUB-.YYYY.-.#####"]
		outstanding_amount: DF.Currency
		plans: DF.Table[SubscriptionPlanDetail]
		print_format: DF.Link | None
		prorate_last_invoice: DF.Check
		recurrence_period: DF.Link
		shipping_rule: DF.Link | None
		start: DF.Date
		status: DF.Literal[
			"",
			"Trial",
			"Active",
			"Cancelled",
			"Unpaid",
			"Paid",
			"Billable",
			"Billing failed",
			"Payable",
			"Draft invoices",
			"Cancelled and billable",
			"Pending",
		]
		subscription_state: DF.JSON | None
		tax_template: DF.Link | None
		terms_and_conditions: DF.Link | None
		total: DF.Currency
		trial_period_end: DF.Date | None
	# end: auto-generated types
	def onload(self):
		SubscriptionPlansManager(self).set_plans_rates()

	def validate(self):
		self.set_invoicing_day()
		self.get_billing_contact()
		self.validate_trial_period()
		SubscriptionPlansManager(self).set_plans_status()
		SubscriptionPlansManager(self).set_plans_rates()
		self.calculate_total()
		self.calculate_grand_total()
		self.calculate_discounts()

	def on_update(self):
		self.set_customer_status()

	def validate_trial_period(self):
		if (
			self.trial_period_end
			and self.current_invoice_start
			and getdate(self.trial_period_end) >= getdate(self.current_invoice_start)
		):
			frappe.throw(_("Trial end date cannot be after the current invoice start date."))

	def calculate_total(self):
		self.total = SubscriptionPlansManager(self).get_plans_total()

	def calculate_grand_total(self):
		self.grand_total = SubscriptionInvoiceGenerator(self).get_simulation()

	def calculate_discounts(self):
		if self.apply_additional_discount:
			total = self.total if self.apply_additional_discount == "Net Total" else self.grand_total
			if self.additional_discount_percentage:
				self.additional_discount_amount = flt(total) * flt(self.additional_discount_percentage) / 100.0
			elif self.additional_discount_amount:
				self.additional_discount_percentage = flt(self.additional_discount_amount) / flt(total) * 100.0

	def process(self):
		SubscriptionPeriod(self).validate()
		SubscriptionPlansManager(self).set_plans_status()
		SubscriptionPlansManager(self).set_plans_rates()
		SubscriptionStateManager(self).set_status()

		self.process_order_and_payment()

		self.set_customer_status()
		automatic_booking_credit_allocation(self)

	def process_order_and_payment(self):
		try:
			if self.status in BILLING_STATUS:
				sales_order = self.generate_sales_order()
				SubscriptionStateManager(self).set_status()

				if (
					sales_order and flt(sales_order.per_billed) < 100.0 and self.generate_invoice_before_payment
				):
					self.generate_invoice(sales_order)
					SubscriptionStateManager(self).set_status()

			if self.status == "Payable":
				SubscriptionPaymentRequestGenerator(self).make_payment_request()

			SubscriptionStateManager(self).set_status()
		except Exception:
			frappe.log_error(_("Subscription update error for subscription {0}").format(self.name))

	def generate_sales_order(self):
		if not (sales_order := self.get_state_value("sales_order")):
			return SubscriptionSalesOrderGenerator(self).create_sales_order()

		return frappe.get_doc("Sales Order", sales_order)

	def generate_invoice(self, sales_order):
		if sales_invoice := self.get_state_value("sales_invoice"):
			return sales_invoice

		try:
			invoice = SubscriptionInvoiceGenerator(self).create_invoice(sales_order)
			self.set_state("sales_invoice", invoice.name)
			invoice.save()
			invoice.submit()
			return invoice
		except Exception as e:
			self.set_state("previous_status", self.status)
			self.db_set("status", "Billing failed")
			self.reload()
			frappe.throw(_("Invoicing error for subscription {0}: {1}").format(self.name, e))

	def cancel_subscription(self, **kwargs):
		self.cancellation_date = kwargs.get("cancellation_date") or self.current_invoice_end
		self.prorate_last_invoice = kwargs.get("prorate_last_invoice")
		self.save(ignore_permissions=kwargs.get("ignore_permissions"))

	def restart_subscription(self):
		self.status = "Active"
		self.current_invoice_start = (
			add_days(self.cancellation_date, 1) if self.cancellation_date else nowdate()
		)
		self.cancellation_date = None
		self.prorate_last_invoice = 0
		self.save()

	def add_plan(self, plan):
		plan_doc = frappe.get_doc("Subscription Plan", plan)
		for plan_line in plan_doc.subscription_plans_template:
			line = copy.deepcopy(plan_line)
			self.append("plans", dict(line.as_dict(), **{"from_date": nowdate(), "name": None}))

	def remove_plan(self, line):
		for plan_line in self.plans:
			if plan_line.name == line:
				plan_line.to_date = nowdate()

	def update_outstanding(self):
		outstanding = sum(
			[
				x.outstanding_amount
				for x in frappe.get_all(
					"Sales Invoice",
					filters={"subscription": self.name, "docstatus": 1},
					fields=["outstanding_amount"],
				)
			]
		)
		self.db_set("outstanding_amount", outstanding)
		SubscriptionStateManager(self).set_status()

	def get_next_invoice_date(self):
		return SubscriptionPeriod(self).get_next_invoice_date()

	def create_payment(self):
		return SubscriptionPaymentEntryGenerator(self).create_payment()

	def create_payment_request(self):
		return SubscriptionPaymentRequestGenerator(self).create_payment_request()

	@frappe.whitelist()
	def link_sales_invoice(self, sales_invoice):
		frappe.db.set_value("Sales Invoice", sales_invoice, "subscription", self.name)

	def set_customer_status(self):
		customer = frappe.get_doc("Customer", self.customer)
		customer.set_status(update=True, update_modified=False)
		if (
			self.get("role_profile_name")
			and customer.meta.has_field("role_profile_name")
			and customer.role_profile_name != self.role_profile_name
		):
			customer.role_profile_name = self.role_profile_name
			customer.save()

	def get_billing_contact(self):
		if self.customer and not self.contact_person:
			self.contact_person = get_default_contact("Customer", self.customer)

	def get_customer_details(self, ignore_permissions=False):
		from erpnext.accounts.party import _get_party_details

		return _get_party_details(
			self.customer,
			ignore_permissions=ignore_permissions,
			doctype="Sales Order",
			company=self.company,
			posting_date=nowdate(),
		)

	def set_invoicing_day(self):
		if not self.invoicing_day or getdate(self.trial_period_end) > getdate(nowdate()):
			self.invoicing_day = (
				getdate(add_days(self.trial_period_end, 1)).day
				if self.trial_period_end
				else (getdate(self.start).day if self.start else None)
			)

		elif (self.get_doc_before_save() or {}).get(
			"recurrence_period"
		) != self.recurrence_period and self.current_invoice_end:
			self.invoicing_day = add_days(getdate(self.current_invoice_end), 1).day

	def set_state(self, key=None, value=None):
		"""
		State values:
		{
		        "previous_status"
		        "sales_order"
		        "sales_invoice"
		        "payment_request"
		        "previous_period_start"
		        "previous_period_end"
		}
		"""
		if not self.subscription_state:
			self.subscription_state = "{}"

		if key:
			state = frappe.parse_json(self.subscription_state)
			state[key] = value
			self.subscription_state = frappe.as_json(state)

		self.db_set("subscription_state", self.subscription_state)

	def get_state_value(self, key):
		state = self.get_state()
		return state.get(key)

	def get_state(self):
		return frappe._dict(frappe.parse_json(self.subscription_state or "{}"))

	def clear_period_state(self):
		for key in ["sales_order", "sales_invoice", "payment_request"]:
			self.set_state(key, "")


def update_grand_total():
	subscriptions = frappe.get_all(
		"Subscription", filters={"status": ("!=", "Cancelled")}, fields=["name", "grand_total"]
	)
	for subscription in subscriptions:
		sub = frappe.get_doc("Subscription", subscription.get("name"))
		previous_total = sub.total
		previous_grand_total = sub.grand_total
		sub.run_method("calculate_total")
		sub.run_method("calculate_grand_total")
		sub.run_method("calculate_discounts")
		if previous_total != sub.total or previous_grand_total != sub.grand_total:
			sub.save()


def process_all():
	subscriptions = frappe.get_all("Subscription", filters={"status": ("!=", "Cancelled")})
	for subscription in subscriptions:
		subscription = frappe.get_doc("Subscription", subscription.name)
		subscription.process()
		frappe.db.commit()


@frappe.whitelist()
def cancel_subscription(**kwargs):
	subscription = frappe.get_doc("Subscription", kwargs.get("name"))
	subscription.cancel_subscription(**kwargs)


@frappe.whitelist()
def restart_subscription(name):
	subscription = frappe.get_doc("Subscription", name)
	subscription.restart_subscription()


@frappe.whitelist()
def get_subscription_updates(name):
	subscription = frappe.get_doc("Subscription", name)
	subscription.process()
	frappe.db.commit()


@frappe.whitelist()
def get_payment_entry(name):
	subscription = frappe.get_doc("Subscription", name)
	return SubscriptionPaymentEntryGenerator(subscription).create_payment()


@frappe.whitelist()
def get_payment_request(name):
	subscription = frappe.get_doc("Subscription", name)
	return SubscriptionPaymentRequestGenerator(subscription).create_payment_request()


@frappe.whitelist()
def get_subscription_plan(plan):
	plan = frappe.get_doc("Subscription Plan", plan)
	meta = frappe.get_meta("Subscription Plan Detail")
	fields = [f.fieldname for f in meta.fields if f.fieldtype not in no_value_fields + default_fields]
	return [{f: p.get(f) for f in fields} for p in plan.subscription_plans_template]


@frappe.whitelist()
def get_chart_data(title, doctype, docname):
	invoices = frappe.get_all(
		"Sales Invoice",
		filters={"subscription": docname, "docstatus": 1},
		fields=["name", "outstanding_amount", "grand_total", "posting_date", "currency"],
		group_by="posting_date DESC",
		limit=20,
	)

	if len(invoices) < 1:
		return {}

	symbol = (
		frappe.db.get_value("Currency", invoices[0].currency, "symbol", cache=True)
		or invoices[0].currency
	)

	dates = []
	total = []
	outstanding = []
	for invoice in invoices:
		dates.insert(0, invoice.posting_date)
		total.insert(0, invoice.grand_total)
		outstanding.insert(0, invoice.outstanding_amount)

	mean_value = np.mean(np.array([x.grand_total for x in invoices]))

	data = {
		"title": title + " (" + symbol + ")",
		"data": {
			"datasets": [
				{"name": _("Invoiced"), "values": total},
				{"name": _("Outstanding Amount"), "values": outstanding},
			],
			"labels": dates,
			"yMarkers": [
				{"label": _("Average invoicing"), "value": mean_value, "options": {"labelPos": "left"}}
			],
		},
		"type": "bar",
		"colors": ["green", "orange"],
	}

	return data


@frappe.whitelist()
def subscription_headline(name):
	subscription = frappe.get_doc("Subscription", name)

	if subscription.cancellation_date and getdate(subscription.cancellation_date) > getdate(
		nowdate()
	):
		return _("This subscription will be cancelled on {0}").format(
			global_date_format(subscription.cancellation_date)
		)
	elif subscription.cancellation_date and subscription.cancellation_date <= getdate(nowdate()):
		return _("This subscription has been cancelled on {0}").format(
			global_date_format(subscription.cancellation_date)
		)
	elif not subscription.status:
		return _(
			"This subscription is not active yet.<br>You can activate it by clicking on 'Fetch Subscription Updates' or wait until the next automatic update (once an hour)."
		)

	next_invoice_date = SubscriptionPeriod(subscription).get_next_invoice_date()
	return (
		_("The next invoice will be generated on {0}").format(global_date_format(next_invoice_date))
		if next_invoice_date
		else ""
	)


@frappe.whitelist()
def new_invoice_end(subscription, end_date):
	new_date = getdate(end_date)
	doc = frappe.get_doc("Subscription", subscription)

	doc.current_invoice_end = new_date
	doc.invoicing_date = add_days(new_date, 1).day
	doc.save()


def get_list_context(context=None):
	context.update(
		{
			"show_sidebar": True,
			"show_search": True,
			"no_breadcrumbs": True,
			"title": _("Subscriptions"),
			"get_list": get_subscriptions_list,
			"row_template": "accounts/doctype/subscription/templates/subscription_row.html",
			"list_template": "accounts/doctype/subscription/templates/subscription_list.html",
			"base_scripts": ["dialog.bundle.js", "controls.bundle.js"],
		}
	)


def get_subscriptions_list(
	doctype, txt, filters, limit_start, limit_page_length=20, order_by=None, ignore_permissions=False
):
	from frappe.www.list import get_list

	from erpnext.controllers.website_list_for_contact import get_customers_suppliers

	customers, _ = get_customers_suppliers(doctype, frappe.session.user)

	if customers and not ignore_permissions:
		ignore_permissions = True

	if not filters:
		filters = []

	filters.extend([("customer", "in", customers)])

	return get_list(
		doctype,
		txt,
		filters=filters,
		fields=[
			"name",
			"status",
			"start",
			"cancellation_date",
			"current_invoice_start",
			"current_invoice_end",
			"recurrence_period",
			"grand_total",
		],
		limit_start=limit_start,
		limit_page_length=limit_page_length,
		ignore_permissions=ignore_permissions,
		order_by="modified desc",
	)


def make_subscription_from_sales_order_item(doc, method):
	if not doc.recurrence_period:
		return

	if doc.doctype != "Sales Order":
		frappe.throw("This function can only be called for a Sales Order")

	if doc.get("subscription"):
		frappe.throw("This Sales Order is already linked to a Subscription")

	# Make a subscription from the Sales Order
	subscription = make_subscription(doc.name, ignore_permissions=True)
	subscription.set_state("sales_order", doc.name)
	subscription.insert(ignore_permissions=True)

	# Link the Sales Order to the Subscription
	doc.subscription = subscription.name
	frappe.db.set_value(doc.doctype, doc.name, "subscription", subscription.name)

	# NOTE: Because the process method will reset the subscription state,
	# which means it will lose track the Sales Order, that was just made,
	# we have to add this flag to avoid the creation of a duplicate Order
	subscription.flags.is_new_subscription = True
	subscription.run_method("process")


def make_subscription(source_name, target_doc=None, ignore_permissions=False) -> Subscription:
	def postprocess(source, target_doc):
		plans_linked_to_recurrence = []
		for plan in target_doc.plans:
			if frappe.db.exists(
				"Item Recurrence Periods",
				dict(parent=plan.item, parenttype="Item", recurrence_period=target_doc.recurrence_period),
			):
				plans_linked_to_recurrence.append(plan)
		target_doc.plans = plans_linked_to_recurrence

	doclist = get_mapped_doc(
		"Sales Order",
		source_name,
		{
			"Sales Order": {
				"doctype": "Subscription",
				"validation": {"docstatus": ["=", 1]},
				"field_map": [
					["from_date", "start"],
					["taxes_and_charges", "tax_template"],
					["name", "sales_order_item"],
				],
			},
			"Sales Order Item": {
				"doctype": "Subscription Plan Detail",
				"field_map": [["item_code", "item"], ["parent", "sales_order"], ["name", "sales_order_item"]],
				"postprocess": update_item,
				"condition": lambda doc: doc.item_code and doc.is_recurring_item,
			},
		},
		target_doc,
		postprocess,
		ignore_permissions=ignore_permissions,
	)

	return doclist


def update_item(obj, target, source_parent):
	if booked_item := frappe.db.get_value("Item", obj.get("item_code"), "booked_item"):
		target.booked_item = booked_item
