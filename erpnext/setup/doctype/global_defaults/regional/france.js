frappe.ui.form.on('Global Defaults', {
	refresh(frm) {
		const pappers_help = frm.get_field("pappers_help")

		let message = `<p>${__("Activate the Pappers API to automatically fetch information about your customers and suppliers.")}</p>`
		message += `<p>${__("Learn more about Pappers on Pappers' website:")} <a href="https://www.pappers.fr" target="_blank">https://www.pappers.fr</a></p>`

		pappers_help.wrapper.innerHTML = message;
	}
})