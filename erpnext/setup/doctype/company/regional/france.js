frappe.ui.form.on("Company", {
	refresh(frm) {
		if (frm.doc.siren_number) {
			frm.add_custom_button(__('Get an RCS extract'), () => {

				const w = window.open(
					frappe.urllib.get_full_url(
						"/api/method/erpnext.regional.france.extensions.company.get_extrait_pappers?" +
						new URLSearchParams({
							siren: frm.doc.siren_number,
						}).toString()
					)
				);
				if (!w) {
					frappe.msgprint(__("Please enable pop-ups"));
					return;
				}
			}, __("Download"))

			frm.add_custom_button(__('Get an INPI extract'), () => {

				const w = window.open(
					frappe.urllib.get_full_url(
						"/api/method/erpnext.regional.france.extensions.company.get_extrait_inpi?" +
						new URLSearchParams({
							siren: frm.doc.siren_number,
						}).toString()
					)
				);
				if (!w) {
					frappe.msgprint(__("Please enable pop-ups"));
					return;
				}
			}, __("Download"))


			frm.add_custom_button(__('Get an INSEE extract'), () => {

				const w = window.open(
					frappe.urllib.get_full_url(
						"/api/method/erpnext.regional.france.extensions.company.get_extrait_insee?" +
						new URLSearchParams({
							siren: frm.doc.siren_number,
						}).toString()
					)
				);
				if (!w) {
					frappe.msgprint(__("Please enable pop-ups"));
					return;
				}
			}, __("Download"))
		}
	}
})